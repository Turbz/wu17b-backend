﻿using MbmStore.Models;
using System;
using System.Web.Mvc;


namespace MbmStore.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {

            
            Customer c1 = new Customer("Ken", "Thompson", "Byagervej 17", "9000", "Aalborg", 1);
            c1.AddPhone("1218 2419");
            c1.AddPhone("2234 4518");
            c1.Birthdate = new DateTime(1987, 4, 6);
            ViewBag.C1 = c1;

            Customer c2 = new Customer("Kirstein", "Roth", "Vibevej 22", "8300", "Odder", 2);
            c2.AddPhone("2214 2818");
            c2.AddPhone("1230 4956");
            c2.Birthdate = new DateTime(1992, 07, 12);
            ViewBag.C2 = c2;

            Customer c3 = new Customer("Karolina", "Laski", "Ponysuperland", "8220", "Brabrand", 3);
            c1.AddPhone("1218 2419");
            c1.AddPhone("2234 4518");
            c1.Birthdate = new DateTime(1996, 9, 13);
            ViewBag.C3 = c3;

            Customer c4 = new Customer("Thorben", "Luepkes", "Superelectronicland", "8220", "Brabrand", 4);
            c2.AddPhone("2214 2818");
            c2.AddPhone("1230 4956");
            c2.Birthdate = new DateTime(1993, 05, 28);
            ViewBag.C4 = c4;

            return View();
        }
    }
}