﻿using MbmStore.Infrastructure;
using MbmStore.Models;
using MbmStore.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using MbmStore.DAL;

namespace MbmStore.Controllers
{
    public class CartController: Controller
    {
        private MbmStoreContext db;
        public CartController()
        {
            db = new MbmStoreContext();
        }

        public PartialViewResult CartItems(Cart cart)
        {
            return PartialView(cart);
        }

        public ViewResult Index(Cart cart, string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }

        public RedirectToRouteResult AddToCart(Cart cart, int productId, string returnUrl)
        {
            Product product = db.Products.FirstOrDefault(p => p.ProductId == productId);
            int quantity = 1;

            if (Request["quantity"] != null)
            {
                quantity = Int32.Parse(Request["quantity"]);
            }

            if (product != null)
            {
                GetCart().AddItem(product, quantity);
            }
            return RedirectToAction("Index", new { controller = returnUrl.Substring(1) });
            
                
        }
        
        public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
        {
            Product product = db.Products.FirstOrDefault(p => p.ProductId == productId);
            if (product != null) { GetCart().RemoveItem(product);
            }
            return RedirectToAction("Index", new { controller = "Cart" });
        }

        private Cart GetCart()
        {
            Cart cart = (Cart)Session["Cart"]; if (cart == null)
            {
                cart = new Cart(); Session["Cart"] = cart;
            }
            return cart;
        }

        public ViewResult Checkout()
        {
            return View(new ShippingDetails());
        }

        [HttpPost]
        public ViewResult Checkout(Cart cart, ShippingDetails shippingDetails)
        {
            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }
            if (ModelState.IsValid)
            {
                // order processing logic
                cart.Clear();
                return View("Completed");
            }
            else
            {
                return View(shippingDetails);
            }
        }



    }
}