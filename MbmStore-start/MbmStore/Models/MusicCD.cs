﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MbmStore.Models
{
    [Table("MusicCD")]
    public class MusicCD : Product
    {
        //create list of tracks
        private List<Track> tracks = new List<Track>();


        //MusicCD properties
        public string Artist { get; set; }
        public string Label { get; set; }
        public short Released { get; set; }

        public virtual List<Track> Tracks
        {
            get { return tracks; }
            set { tracks = value; }
        }




        //Music CD constructer
        public MusicCD() { }

        public MusicCD(int productId, string artist, string title, string imageurl, decimal price, short released) : base(title, price, productId)
        {

            Artist = artist;
            ImageUrl = imageurl;         
            Released = released; 
            Tracks = tracks;
        }


        //Add Track Method
        public void AddTrack(Track track)
        {
            Tracks.Add(track);
        }

        // TimeSpam Method
        public TimeSpan GetPlayingTime()
        {
            TimeSpan playingTime = new TimeSpan(0, 0, 0);

            foreach (Track track in tracks)
            {
                playingTime += track.Length;
            }

            return playingTime;
        }

    }
}