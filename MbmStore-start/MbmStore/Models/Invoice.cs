﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace MbmStore.Models
{
    public class Invoice
    {
        // private List<string> orderItems = new List<string>();


        public int InvoiceID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime OrderDate { get; set; }
        public int CustomerCustomerId { get; set; }

        public Customer Customer { get; set; }
        public decimal GetTotalPrice()
        {
            decimal totalPrice = 0m;
            decimal sp = 0m;


            foreach (OrderItem item in OrderItems)
            {
                sp = item.Product.Price * item.Quantity;
                totalPrice += sp;
            }
            return totalPrice;
        }


        //Constructor
        private List<OrderItem> orderItems = new List<OrderItem>();
        public List<OrderItem> OrderItems { get { return orderItems; } set { orderItems = value; } }

        public Invoice() { }

        public Invoice(int invoiceID, DateTime orderDate, Customer customer)
        {
            InvoiceID = invoiceID;
            OrderDate = orderDate;
            Customer = customer;
        }

        // Method
        public void AddOrderItem(Product product, int quantity)        {

            OrderItem item = orderItems.Where(p => p.Product.ProductId == product.ProductId).FirstOrDefault();

            if (item == null)
            {
                orderItems.Add(new OrderItem(product, quantity));
            }
            else
            {
                item.Quantity += quantity;
            }
        }


    }
}