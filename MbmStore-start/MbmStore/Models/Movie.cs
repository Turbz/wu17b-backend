﻿using System.ComponentModel.DataAnnotations.Schema;


namespace MbmStore.Models
{
    [Table("Movie")]
    public class Movie : Product
    {

        
        private string director;

        public string Director
        {
            get { return director; } 
            set { director = value; } 
        }

        // constructors
        public Movie() { }

        public Movie (int productId, string title, decimal price, string imageUrl, string director) : base(title, price, productId)
        {
            ImageUrl = imageUrl;
            Director = director;
      
        }
    }
}