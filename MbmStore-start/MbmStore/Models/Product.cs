﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MbmStore.Models
{
    public class Product
    {
        //General Properties
        public int ProductId { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public string Category { get; set; }

        public Product() { }

        //Constructor
        public Product(string title, decimal price, int productId) {
            Title = title;
            Price = price;
            ProductId = productId;
        }

        //Constructor 2 to inherit into MusicCDs
        public Product(decimal price)
        {
            Price = price;
        }

    }
}