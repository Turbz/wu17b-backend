﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MbmStore.Models
{
    public class User
    {
        [Required]
        public string Name { get; set; }
        [Required(ErrorMessage ="Du skal have et Brugernavn")]
        [StringLength(50, MinimumLength = 4)]
        public string UserName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 6)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage ="de skal være ens")]
        public string ConfirmPassword { get; set; }
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage ="Insert Correct Email")]
        public string Email { get; set; }

        public int UserId { get; set; }

        private DateTime birthdate;


        [Column(TypeName = "datetime2")]
        public DateTime Birthdate
        {
            set
            {
                if ((DateTime.Now.Year - value.Year) > 120 ||
                    (DateTime.Now.Year - value.Year) < 0)
                {
                    throw new Exception("Age is not accepted");
                }
                else
                {
                    birthdate = value;
                }
            }
            get { return birthdate; }
        }


        // read only property for Age
        public int Age
        {
            get
            {
                DateTime now = DateTime.Now;
                int age = 0;
                age = now.Year - birthdate.Year;
                if (now.Month < birthdate.Month ||
                    (now.Month == birthdate.Month && now.Day < birthdate.Day))
                {
                    age--;
                }
                return age;
            }
        }

        public User ()
        {

        }

        public User(string name, string username, string password, string confirmPassword, DateTime birthdate, int userId, string email)
        {

            Name = name;
            UserName = username;
            Password = password;
            ConfirmPassword = confirmPassword;
            UserId = userId;
            Birthdate = birthdate;
            Email = email;
        }

    }
}