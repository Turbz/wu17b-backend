﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MbmStore.Models
{
    public class Customer
    {
        private DateTime birthdate;
        private List<string> phoneNumbers = new List<string>();
        public List<Invoice> Invoices = new List<Invoice>();
        public virtual ICollection<Phone> PhoneNumbers { get; set; }


        public int CustomerId { get; set; }
        public string Firstname { get; set;  }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }



        // property that validates input (the set accessor)
        [Column(TypeName = "datetime2")]
        public DateTime Birthdate
        {
            set
            {
                if ((DateTime.Now.Year - value.Year) > 120 ||
                    (DateTime.Now.Year - value.Year) < 0)
                {
                    throw new Exception("Age is not accepted");
                }
                else {
                    birthdate = value;
                }
            }
            get { return birthdate; }
        }


        // read only property for Age
        public int Age
        {
            get
            {
                DateTime now = DateTime.Now;
                int age = 0;
                age = now.Year - birthdate.Year;
                if (now.Month < birthdate.Month ||
                    (now.Month == birthdate.Month && now.Day < birthdate.Day))
                {
                    age--;
                }
                return age;
            }
        }



        // cunstructor

        public Customer() { }

        public Customer(string firstnavn, string lastnavn, string address, string zip, string city, int customerId)
        {
            
            Firstname = firstnavn;
            Lastname = lastnavn;
            Address = address;
            Zip = zip;
            City = city;
            CustomerId = customerId;   
        }

        // method
        public void AddPhone(string phone)
        {
            phoneNumbers.Add(phone);
        }

        // method
        public void AddInvoice(Invoice invoice)
        {
            Invoices.Add(invoice);
        }
    }



}