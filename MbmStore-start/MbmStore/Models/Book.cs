﻿using System.ComponentModel.DataAnnotations.Schema;


namespace MbmStore.Models
{
    [Table("Book")]
    public class Book : Product 
    {

        public string Author { get; set; }
        public string Publisher { get; set; }
        public short Published { get; set; }
        public string ISBN { get; set; }
        public string tmp { get; set; }


        public Book() { }

        public Book(int productId, string author, string title, string imageUrl, decimal price, short published) : base(title, price, productId)
        {
            Author = author;
            ImageUrl = imageUrl;
            Published = published;
        }

    }
}