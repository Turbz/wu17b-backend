namespace MbmStore.Migrations
{
    using MbmStore.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MbmStore.DAL.MbmStoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MbmStore.DAL.MbmStoreContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            // Add Products to a list
            var products = new List<Product>
            {

                //Books
                new Book {ProductId=1, Title="The Art of C#", Author="Thorben and Patrick", Price=19.00M, Publisher="BAAA Books",
                        Published=2017, ISBN="978-0060844097", ImageUrl="book1.jpg", Category="Book"},
                new Book {ProductId=2, Title="The Art of SASS", Author="Thorben and Patrick", Price=19.00M, Publisher="BAAA Books",
                        Published=2017, ISBN="978-0060844098", ImageUrl="book3.jpg", Category="Book"},
                new Book {ProductId=3, Title="The Art of Laravel", Author="Thorben and Patrick", Price=19.00M, Publisher="BAAA Books",
                        Published=2017, ISBN="978-0060844298", ImageUrl="book2.jpg", Category="Book"},

                // CD Objects
                new MusicCD {ProductId=4, Title="You never walk alone", Artist="BTS", Price=15.00m, Label="BigHit", ImageUrl="ynwa.jpg", Category="MusicCD",
                                                    Tracks = new List<Track> { new Track {Title="Spring Day", Length=new TimeSpan(0, 4, 20),Composer="Pdoogg"},
                                                                            }
                            },
                new MusicCD {ProductId=5, Title="Love yourself: HER", Artist="BTS", Price=15.00m, Label="BigHit", ImageUrl="dna.jpg", Category="MusicCD",
                                        Tracks = new List<Track> { new Track {Title="DNA", Length=new TimeSpan(0, 4, 20),Composer="Pdoogg, SUGA"},
                                                                }
                },

                //Movies

                new Movie {ProductId=6, Title="Tokyou Drift", Price=160.50M, ImageUrl="junglebook.jpg", Director="Idk", Category="Movie"},
                new Movie {ProductId=7, Title="Transformers 2", Price=160.50M, ImageUrl="transformers.jpg", Director="Some Japanese guy", Category="Movie"},
                new Movie {ProductId=8, Title="Sword Art Online - Ordinal Scale", Price=160.50M, ImageUrl="sao.jpg", Director="Some Japanese guy", Category="Movie"},

            };



            // Populate the database with desired products
            products.ForEach(p => context.Products.Add(p));
            context.SaveChanges();



            //---------------------------------------------//
            //---------------------------------------------//
            //---------------------------------------------//

            // Add Customers to a list
            var customers = new List<Customer>
             {

             new Customer {CustomerId=1, Firstname="Tina",Lastname="Petterson",Address="Irisdahlsvej 32", Zip="8200", City="�rhus N"},
             new Customer {CustomerId=2, Firstname="Patrick", Lastname="Mortebseb",Address="Whereever", Zip="8000", City="�rhus C"},
             new Customer {CustomerId=3, Firstname="Thorben",Lastname="Luepkes",Address="Brabrand", Zip="8200", City="�rhus N"},
             new Customer {CustomerId=4, Firstname="Karol",Lastname="Laski",Address="Brabrand", Zip="8200", City="�rhus N"},
             new Customer {CustomerId=5, Firstname="Bob", Lastname="Laumann",Address="Kolding", Zip="8000", City="�rhus C"}

             };





            // Populate the database with our customers
            customers.ForEach(c => context.Customers.Add(c));
            context.SaveChanges();


            //---------------------------------------------//
            //---------------------------------------------//
            //---------------------------------------------//


            // Add Invoices to a list
            var invoices = new List<Invoice>
            {
                 new Invoice {InvoiceID=1, OrderDate=new DateTime(2016, 09, 12), CustomerCustomerId=3,
                     OrderItems = new List<OrderItem> { new OrderItem {ProductID=7, Quantity=1},
                                                          new OrderItem {ProductID=2, Quantity=4}
                                                      }

                 },
                 new Invoice {InvoiceID=2, OrderDate=new DateTime(2016, 09, 10), CustomerCustomerId=4,
                     OrderItems = new List<OrderItem> { new OrderItem {ProductID=7, Quantity=1},
                                                          new OrderItem {ProductID=2, Quantity=4}
                                                      }

                 },
            };

            // populate the database with invoices
            invoices.ForEach(i => context.Invoices.Add(i));
            context.SaveChanges();


            //---------------------------------------------//
            //---------------------------------------------//
            //---------------------------------------------//
        }
    }
}
