namespace MbmStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigrationUserEmail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ImageUrl = c.String(),
                        Category = c.String(),
                    })
                .PrimaryKey(t => t.ProductId);
            
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        Address = c.String(),
                        Zip = c.String(),
                        City = c.String(),
                        Birthdate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Phone",
                c => new
                    {
                        PhoneId = c.Int(nullable: false, identity: true),
                        Number = c.String(),
                        CustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PhoneId)
                .ForeignKey("dbo.Customer", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Invoice",
                c => new
                    {
                        InvoiceID = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CustomerCustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InvoiceID)
                .ForeignKey("dbo.Customer", t => t.CustomerCustomerId, cascadeDelete: true)
                .Index(t => t.CustomerCustomerId);
            
            CreateTable(
                "dbo.OrderItem",
                c => new
                    {
                        OrderItemID = c.Int(nullable: false, identity: true),
                        ProductID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Invoice_InvoiceID = c.Int(),
                    })
                .PrimaryKey(t => t.OrderItemID)
                .ForeignKey("dbo.Product", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.Invoice", t => t.Invoice_InvoiceID)
                .Index(t => t.ProductID)
                .Index(t => t.Invoice_InvoiceID);
            
            CreateTable(
                "dbo.Track",
                c => new
                    {
                        TrackId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Length = c.Time(nullable: false, precision: 7),
                        Composer = c.String(),
                        MusicCD_ProductId = c.Int(),
                    })
                .PrimaryKey(t => t.TrackId)
                .ForeignKey("dbo.MusicCD", t => t.MusicCD_ProductId)
                .Index(t => t.MusicCD_ProductId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 50),
                        Password = c.String(nullable: false, maxLength: 50),
                        ConfirmPassword = c.String(),
                        Email = c.String(),
                        Birthdate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Book",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        Author = c.String(),
                        Publisher = c.String(),
                        Published = c.Short(nullable: false),
                        ISBN = c.String(),
                        tmp = c.String(),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Movie",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        Director = c.String(),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.MusicCD",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        Artist = c.String(),
                        Label = c.String(),
                        Released = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MusicCD", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Movie", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Book", "ProductId", "dbo.Product");
            DropForeignKey("dbo.OrderItem", "Invoice_InvoiceID", "dbo.Invoice");
            DropForeignKey("dbo.OrderItem", "ProductID", "dbo.Product");
            DropForeignKey("dbo.Track", "MusicCD_ProductId", "dbo.MusicCD");
            DropForeignKey("dbo.Invoice", "CustomerCustomerId", "dbo.Customer");
            DropForeignKey("dbo.Phone", "CustomerId", "dbo.Customer");
            DropIndex("dbo.MusicCD", new[] { "ProductId" });
            DropIndex("dbo.Movie", new[] { "ProductId" });
            DropIndex("dbo.Book", new[] { "ProductId" });
            DropIndex("dbo.Track", new[] { "MusicCD_ProductId" });
            DropIndex("dbo.OrderItem", new[] { "Invoice_InvoiceID" });
            DropIndex("dbo.OrderItem", new[] { "ProductID" });
            DropIndex("dbo.Invoice", new[] { "CustomerCustomerId" });
            DropIndex("dbo.Phone", new[] { "CustomerId" });
            DropTable("dbo.MusicCD");
            DropTable("dbo.Movie");
            DropTable("dbo.Book");
            DropTable("dbo.User");
            DropTable("dbo.Track");
            DropTable("dbo.OrderItem");
            DropTable("dbo.Invoice");
            DropTable("dbo.Phone");
            DropTable("dbo.Customer");
            DropTable("dbo.Product");
        }
    }
}
