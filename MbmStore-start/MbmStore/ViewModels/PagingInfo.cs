﻿using System;
using System.Text;
using System.Web.Mvc;

namespace MbmStore.ViewModels
{
    public class PagingInfo
    {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }
        public double TotalPages
        {
            get
            {
                double totalPages = (double)TotalItems / (double)ItemsPerPage;
                return Math.Ceiling(totalPages);
            }

        }
    }
}