Mandatory Assignment 2: ASP.NET MVC web site with shopping cart The 

Product:

In this assignment you�ll develop and implement a design and a shopping cart for the MbmStore website. 
To do that you must follow the instructions of exercise 1-7 in lesson 6 and exercise 1-5 in lesson 7.

The report: (google Docs)

I addition to the product you must write a report that documents the product and explains how you have used the ASP.NET MVC framework.
In the report, you�re supposed to explain the following topics and demonstrate how they are implemented in the solution:

	- The MVC application architecture (Patrick)
	- Forms and Input Helpers and Templated Helpers in views (Bob)
	- Routing and model binding (Thorben)
	- Views, layout pages, partial views and child actions (Thorben)
	- Strongly typed views and ViewModels (Patrick)

The report must also include:

	- An overview of the classes used in the solution presented as an UML diagram. (Bob)

Important information must be documented with footnotes. Use Adam Freeman: "Pro ASP.NET MVC 5" 5th edition, as your main reference.
There must be:

	- An initial section with an introduction to the project and the report
	- A conclusion with summary and evaluation and reflections on the learning process

Size: 
Maximum 12 pages (one standard page being 2400 characters, spaces included), 
and maximum three persons per project. The report should be paginated.
Handing-in

The assignment � the ASP.NET MVC project plus the report � must be uploaded to Canvas as a .zip file named with your own name or if you work in a group, the names of the group members.
The assignment must be approved in order to be recommended for examination in the Backend Programming module.

Deadline:
WU-17b: Friday, October 29